package org.anyline.test;

import org.anyline.entity.DataSet;
import org.anyline.proxy.ServiceProxy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"org.anyline"}) //这个写不写都可以 如果写了的话一定我包含自己项目的包名
@SpringBootApplication
public class AnylineTestApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(AnylineTestApplication.class);
        application.run(args);
        DataSet set = ServiceProxy.querys("CRM_USER");
        System.out.print(set.size());
        System.out.print(set.toJSON());
        System.exit(0);
    }

}
